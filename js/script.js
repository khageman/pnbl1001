(function(){
    document.addEventListener('DOMContentLoaded', function() {

        var siteData = [];

        // $.getJSON('../data/projects.json', function(data) { 
            
        // }).done(function(data) {
        //     siteData = siteData.concat(data.projects);
        //     projectsTemplate = Handlebars.getTemplate("projects");
        //     projects = projectsTemplate(data);
        //     $("#projects").html(projects);
        //     projectNavItems = document.querySelectorAll(".project-preview");
        //     for(var i = 0; i < projectNavItems.length; i++) {
        //         var elem = projectNavItems[i];
        //         elem.addEventListener('click', function(e) {
        //             var relatedData = [];
        //             for(var j = 0; j < data.projects.length; j++) {
        //                 if(data.projects[j].id === $(e.currentTarget).data('id') ) {
        //                     var matchedData = data.projects[j];
        //                 } else if(data.projects[j].id && relatedData.length < 4){
        //                     relatedData.push(data.projects[j]);
        //                 }
        //             }
        //             var templateObj = {
        //                 "project": matchedData,
        //                 "projects": relatedData

        //             }
        //             showProject(templateObj);

        //         });
        //     }
        // }).fail(function(xhr) {
        //     console.log('fail');
        //     console.log(xhr.responseText);
        // }).always(function() {
        //     console.log('always');
        // });

        // $.getJSON('../data/words.json', function(data) { 
            
        // }).done(function(data) {
        //     siteData = siteData.concat(data.words);
        //     wordsTemplate = Handlebars.getTemplate("words");
        //     words = wordsTemplate(data);
        //     $("#words").html(words);
        //     wordItems = document.querySelectorAll("#words article");
        //     var wordType = undefined;
        //     for(var i = 0; i < wordItems.length; i++) {
        //         var elem = wordItems[i];
        //         if($(elem).hasClass('simple')) { // TODO: fix access to properties/data through html elements vs json data
        //             elem.addEventListener('click', function(e) {
        //                 var relatedData = [];
        //                 for(var j = 0; j < data.words.length; j++) {
        //                     if(data.words[j].id === $(e.currentTarget).data('id') ) {
        //                         var matchedData = data.words[j];
        //                     } else if(relatedData.length < 4) {
        //                         relatedData.push(data.words[j]);
        //                     }
        //                 }
        //                 var templateObj = {
        //                     "word": matchedData,
        //                     "words": relatedData

        //                 }
        //                 showWord(templateObj, 'simple');
        //             });
        //         } else {
        //             elem.addEventListener('click', function(e) {
        //                 var relatedData = [];
        //                 for(var j = 0; j < data.words.length; j++) {
        //                     if(data.words[j].id === $(e.currentTarget).data('id') ) {
        //                         var matchedData = data.words[j];
        //                     } else if(relatedData.length < 4) {
        //                         relatedData.push(data.words[j]);
        //                     }
        //                 }
        //                 var templateObj = {
        //                     "word": matchedData,
        //                     "words": relatedData

        //                 }
        //                 showWord(templateObj, 'standard');
        //             });
        //         }
        //     }
        // }).fail(function(xhr) {
        //     console.log('fail');
        //     console.log(xhr.responseText);
        // }).always(function() {
        //     console.log('always');
        // });

        Handlebars.getTemplate = function(name) {
            if (Handlebars.templates === undefined || Handlebars.templates[name] === undefined) {
                $.ajax({
                    url : 'html/templates/' + name + '.handlebars',
                    success : function(data) {
                        if (Handlebars.templates === undefined) {
                            Handlebars.templates = {};
                        }
                    Handlebars.templates[name] = Handlebars.compile(data);
                    },
                async : false
                });
            }
            return Handlebars.templates[name];
        };

        Handlebars.getPartial = function(name) {
            if (Handlebars.partials === undefined || Handlebars.partials[name] === undefined) {
                $.ajax({
                    url : 'html/partials/' + name + '.handlebars',
                    success : function(data) {
                        if (Handlebars.partials === undefined) {
                            Handlebars.partials = {};
                        }
                    Handlebars.partials[name] = Handlebars.compile(data);
                    },
                async : false
                });
            }
            return Handlebars.partials[name];
        };

        var self = this;

        var mqLarge = window.matchMedia('(min-width: 960px)');

        var mainNavPartial = Handlebars.getPartial("main-nav");
        Handlebars.registerPartial('mainNav', mainNavPartial);

        var footerPartial = Handlebars.getPartial("footer");
        Handlebars.registerPartial('footer', footerPartial);

        var pages = ['styles', 'home', 'about', 'donate', 'impact', 'getInvolved', 'news', 'events', 'post', 'store']; // DO: iterate on this to do templates based on this array of tempalate/page names.



        if($("#styles").length > 0) {
            var stylesTemplate = Handlebars.getTemplate("styles");
            var styles = stylesTemplate();
            $("#styles").html(styles);
        }

        if($("#head").length > 0) {
            var headTemplate = Handlebars.getTemplate("head");
            var head = headTemplate();
            $("#head").html(head);
        }

        if($("#home").length > 0) {
            var homeTemplate = Handlebars.getTemplate("home");
            var home = homeTemplate();
            $("#home").html(home);
        }

        if($("#about").length > 0) {
            var aboutTemplate = Handlebars.getTemplate("about");
            var about = aboutTemplate();
            $("#about").html(about);
        }

        if($("#donate").length > 0) {
            var donateTemplate = Handlebars.getTemplate("donate");
            var donate = donateTemplate();
            $("#donate").html(donate);
        }

        if($("#impact").length > 0) {
            var impactTemplate = Handlebars.getTemplate("impact");
            var impact = impactTemplate();
            $("#impact").html(impact);
        }

        if($("#get-involved").length > 0) {
            var getInvolvedTemplate = Handlebars.getTemplate("get-involved");
            var getInvolved = getInvolvedTemplate();
            $("#get-involved").html(getInvolved);
        }

        if($("#news").length > 0) {
            var newsTemplate = Handlebars.getTemplate("news");
            var news = newsTemplate();
            $("#news").html(news);
        }

        if($("#events").length > 0) {
            var eventsTemplate = Handlebars.getTemplate("events");
            var events = eventsTemplate();
            $("#events").html(events);
        }

        if($("#post").length > 0) {
            var postTemplate = Handlebars.getTemplate("post");
            var post = postTemplate();
            $("#post").html(post);
        }

        if($("#store").length > 0) {
            var storeTemplate = Handlebars.getTemplate("store");
            var store = storeTemplate();
            $("#store").html(store);
        }

        if($("#sweepstakes").length > 0) {
            var sweepstakesTemplate = Handlebars.getTemplate("sweepstakes");
            var sweepstakes = sweepstakesTemplate();
            $("#sweepstakes").html(sweepstakes);
        }

        if($("#sponsors").length > 0) {
            var sponsorsTemplate = Handlebars.getTemplate("sponsors");
            var sponsors = sponsorsTemplate();
            $("#sponsors").html(sponsors);
        }

        if($("#contact").length > 0) {
            var contactTemplate = Handlebars.getTemplate("contact");
            var contact = contactTemplate();
            $("#contact").html(contact);
        }

        if($("#hospital").length > 0) {
            var hospitalTemplate = Handlebars.getTemplate("hospital");
            var hospital = hospitalTemplate();
            $("#hospital").html(hospital);
        }

        if($("#event").length > 0) {
            var eventTemplate = Handlebars.getTemplate("event");
            var event = eventTemplate();
            $("#event").html(event);
        }

        if($("#store-item").length > 0) {
            var storeItemTemplate = Handlebars.getTemplate("store-item");
            var storeItem = storeItemTemplate();
            $("#store-item").html(storeItem);
        }

        if($("#event-registration").length > 0) {
            var eventRegistrationTemplate = Handlebars.getTemplate("event-registration");
            var eventRegistration = eventRegistrationTemplate();
            $("#event-registration").html(eventRegistration);
        }

        if($("#theme-menu").length > 0) {
            $('#theme-1-button').on('click', function() {
                console.log('change theme');
                document.querySelectorAll("body")[0].classList.add("theme-3");
                document.querySelectorAll("body")[0].classList.remove("theme-1");
                document.querySelectorAll("body")[0].classList.remove("theme-2");
                document.querySelectorAll("body")[0].classList.remove("theme-4");
                document.getElementById("style").setAttribute("href", 'css/style1.css');
            });

            $('#theme-2-button').on('click', function() {
                console.log('change theme');
                document.querySelectorAll("body")[0].classList.add("theme-2");
                document.querySelectorAll("body")[0].classList.remove("theme-1");
                document.querySelectorAll("body")[0].classList.remove("theme-3");
                document.querySelectorAll("body")[0].classList.remove("theme-4");
                document.getElementById("style").setAttribute("href", 'css/style2.css');
            });

            $('#theme-3-button').on('click', function() {
                console.log('change theme');
                document.querySelectorAll("body")[0].classList.add("theme-3");
                document.querySelectorAll("body")[0].classList.remove("theme-1");
                document.querySelectorAll("body")[0].classList.remove("theme-2");
                document.querySelectorAll("body")[0].classList.remove("theme-4");
                document.getElementById("style").setAttribute("href", 'css/style3.css');
            });
        }

        // var introTemplate = Handlebars.getTemplate("intro");
        // var intro = introTemplate();
        // $("#intro").html(intro);

        // var aboutTemplate = Handlebars.getTemplate("about");
        // var about = aboutTemplate();
        // $("#about").html(about);

        // var contactTemplate = Handlebars.getTemplate("contact");
        // var contact = contactTemplate();
        // $("#contact").html(contact);

        // var lifeAndTimesTemplate = Handlebars.getTemplate("life-and-times");
        // var lifeAndTimes = lifeAndTimesTemplate();
        // $("#life-and-times").html(lifeAndTimes);

        // var projectViewTemplate = Handlebars.getTemplate("project-view");
        // var projectView2 = projectViewTemplate(self.context);
        // $("#project-view").html(projectView2);

        // var wordViewTemplate = Handlebars.getTemplate("word-view");
        // var wordView2 = wordViewTemplate(this.context);
        // $("#word-view").html(wordView2);


        // var mainNavTemplate = Handlebars.getTemplate("main-nav");
        // var mainNav = mainNavTemplate();
        // $("#main-nav").html(mainNav);

        // var searchTemplate = Handlebars.getTemplate("search");
        // var search = searchTemplate();
        // $("#search").html(search);

        // var footerTemplate = Handlebars.getTemplate("footer");
        // var footer = footerTemplate();
        // $("#footer").html(footer);

        var body = document.querySelectorAll("body")[0],
            bodyHtml = $("body, html"),
            mainNav = document.querySelectorAll("#main-nav")[0],
            toggleNavButton = document.querySelectorAll("#toggle-nav-button")[0];

        // searchInput.addEventListener('input', function(e) {
        //     var query = e.currentTarget.value;
        //     search(query);
        // });

        toggleNavButton.addEventListener('click', function(e) {
            console.log('hey');
            body.classList.toggle('show-main-nav');
        });

        function search(string) {
            console.log(siteData);
            var results = [];
            searchLoop1: for (var i = 0, len = siteData.length; i < len; i++) {
                if(siteData[i].title.toLowerCase().indexOf(string) > -1 || siteData[i].subtitle.toLowerCase().indexOf(string) > -1) {
                    results.push(siteData[i]);   
                } else if(siteData[i].tags && siteData[i].tags.length > 0 ) {
                    searchLoop2: for (var j = 0, len2 = siteData[i].tags.length; j < len2; j++) {
                        if(siteData[i].tags[j].name.toLowerCase().indexOf(string) > -1) {
                            results.push(siteData[i]);
                            break;
                        }
                    }
                }
            }

            if(string === '') {
                results = [];
            }

            // console.log(results);
            // searchResultsList.innerHTML = results;

            var resultsObj = {
                "results": results
            }

            var searchResultsTemplate = Handlebars.getTemplate("search-results");
            var searchResults = searchResultsTemplate(resultsObj);
            searchResultsList.innerHTML = searchResults;
            if(resultsObj.results.length > 0) {
                searchResultsList.classList.add("results");
            } else {
                searchResultsList.classList.remove("results");
            }
        }

        // function showProject(project) {
        //     var context = project;

        //     var projectView2 = projectViewTemplate(context);
        //     $("#project-view").html(projectView2);
            
        //     sections.style["overflow"] = "hidden";
        //     // feature.style["display"] = "block";
        //     projectView.classList.add("show");
        //     body.style.overflow = "hidden";
        //     sections.classList.add("hide");

        //     hideProjectButton = document.querySelectorAll("#close-project")[0];

        //     hideProjectButton.addEventListener('click', function(e) {
        //         hideFeature();
        //     });
        // }

        // function showWord(obj, wordType) {

        //     console.log(wordType);

        //     var context = obj;

        //     var wordView2 = wordViewTemplate(context);
        //     $("#word-view").html(wordView2);

        //     if(wordType === 'simple') {
        //         console.log('simple')
        //         $("#word-view").removeClass('standard');
        //         $("#word-view").addClass('simple');
        //     } else {
        //         console.log('standard');
        //         $("#word-view").removeClass('simple');
        //         $("#word-view").addClass('standard');
        //     }

        //     sections.style["overflow"] = "hidden";
        //     // feature.style["display"] = "block";
        //     wordView.classList.add("show");
        //     body.style.overflow = "hidden";
        //     sections.classList.add("hide");

        //     hideWordButton = document.querySelectorAll("#close-word")[0],

        //     hideWordButton.addEventListener('click', function(e) {
        //         hideFeature();
        //     });
        // }

        // function hideFeature() {
        //     // feature.style["display"] = "none";
        //     projectView.classList.remove("show");
        //     wordView.classList.remove("show");
        //     sections.style["overflow"] = "auto";
        //     body.style.overflow = "auto";
        //     sections.classList.remove("hide");
        // }

        function closeNav() {

        }


        // hideMainNavButton.addEventListener('click', function(e) {
        //     body.classList.toggle("hide-menu");
        //     window.clearTimeout(masonryTimer);
        //     var masonryTimer = undefined;
        //     var callMasonry = function() {
        //         $('.posts').masonry({
        //             columnWidth: '.post',
        //             itemSelector: '.post',
        //             percentPosition: true,
        //             gutter: 10
        //         });
        //     }
        //     masonryTimer = window.setTimeout(callMasonry, 500);
        // });


        // Gamepad Support
        // window.addEventListener("gamepadconnected", function(e) {
            // console.log("Gamepad connected at index %d: %s. %d buttons, %d axes.",
            // e.gamepad.index, e.gamepad.id,
            // e.gamepad.buttons.length, e.gamepad.axes.length);
        // });

        var gamepads = {};

        function gamepadHandler(event, connecting) {
          var gamepad = event.gamepad;
          // Note:
          // gamepad === navigator.getGamepads()[gamepad.index]

          if (connecting) {
            gamepads[gamepad.index] = gamepad;
          } else {
            delete gamepads[gamepad.index];
          }
        }

        // window.addEventListener("gamepadconnected", function(e) { gamepadHandler(e, true); }, false);
        // window.addEventListener("gamepaddisconnected", function(e) { gamepadHandler(e, false); }, false);

        var timeout = window.setTimeout(function() {
          $('a').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
              var target = $(this.hash);
              target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
              if (target.length) {
                if(mqLarge.matches) {
                    // the width of browser is more then 700px
                    var elemToScroll = bodyHtml;
                } else {
                    // the width of browser is less then 700px
                    var elemToScroll = bodyHtml;
                }
                $(elemToScroll).animate({
                  scrollTop: $(elemToScroll).scrollTop() + target.offset().top
                }, 400, 'swing');
                return false;
              }
            }
          });
        }, 500);

        FastClick.attach(document.body);

        window.onload = function() {
            // $('.posts').masonry({
            //   columnWidth: '.post',
            //   itemSelector: '.post',
            //   percentPosition: true,
            //   gutter: 10
            // });
        }

        var parallaxImg1 = $($('.parallax-img')[0]);
        var parallaxImg2 = $($('.parallax-img.two')[0]);
        var parallaxImg3 = $($('.parallax-img.three')[0]);
        // var parallaxImg3 = $($('.parallax-img.three')[0]);
        var parallaxText = $($('.parallax-text')[0]);
        // var parallaxImg2 = $($('.parallax-img')[1]);

        var scrollElem = undefined;

        function setScrollElem() {
            if(mqLarge.matches) {
                scrollElem = window;
            } else {
                scrollElem = window;
            }
        }





        // var didScroll = false;

        // window.onscroll = doThisStuffOnScroll;

        // function doThisStuffOnScroll() {
        //     didScroll = true;
        // }

        // setInterval(function() {
        //     if(didScroll) {
        //         didScroll = false;
        //         // window.requestAnimationFrame(scrolled);
        //     }
        // }, 100);




        function scrolled() {

            setScrollElem();

            if(parallaxImg1.offset().top + parallaxImg1.height() > 0) {
                var yPos1 = ($(scrollElem).scrollTop() / 10).toFixed(2);
                    yPos2 = (yPos1 * 3).toFixed(2),
                    yPos3 = (yPos1 * 2).toFixed(2),
                    yPos4 = (yPos1 * 5).toFixed(2);
                    
                
                // Put together our final background position
                var coords = 'center calc(-50px + '+ yPos1 + 'px)';

                // Move the background
                // if(self.slideNum === 1) {
                    // parallaxImg1.css({ transform: 'translate3d(-50%, ' + yPos1 + 'px, 0px)' });
                    // parallaxImg2.css({ transform: 'translate3d(-50%, ' + yPos3 + 'px, 0px)' });
                    // parallaxImg3.css({ transform: 'translate3d(-50%, ' + yPos4 + 'px, 0px)' });
                    // parallaxText.css({ transform: 'translate3d(-50%, calc(-50% + ' + yPos2 + 'px), 0px)' });
                // } else if(self.slideNum === 2) {
                    // parallaxImg2.css({ transform: 'translate3d(-50%, calc(-50% + ' + yPos + 'px)), 0px' });
                // }
            }
        }
        

        function scrollListener() {

            setScrollElem();

            $(scrollElem).scroll(function() {
                // didScroll = true;
                // window.requestAnimationFrame(scrolled);
            });
        }

        scrollListener();

        window.onresize = function() {
            var mqLarge = window.matchMedia('(min-width: 960px)');
            scrollListener();
        }
        
    }); // DOMContentLoaded

    var self = this;

    this.slideTotal = 2;
    this.slideNum = 1;

    $('#intro').data('slide', 1);

    // var introInterval = window.setInterval(changeSlide, 3000);

    function changeSlide() {
        if(self.slideNum < self.slideTotal) {
            self.slideNum = self.slideNum + 1;
        } else {
            self.slideNum = 1;
        }

        $('#intro').attr('data-slide', self.slideNum);
    }

    document.body.addEventListener('touchstart', function(e){
        document.body.addEventListener('touchend', function(e2){
            // $('*').css({'background': 'red'});
            // var e = document.createEvent('TouchEvent');
            // e.initTouchEvent();
            var hoverTimeout = window.setTimeout(function() {
                // $('.fix-hover').trigger( "click" );
                // $('*').css({'background': 'red'});
                $(e).blur();
                // $('.fix-hover').blur();
                // $('*').css({'background': 'red'});
            }, 500);
        }, false);
    }, false);

    console.log("Hey");

    // var test = $('.fix-hover');
    // console.log(test);

    // Fix shitty iOS sitkcy :hover sytles
    function removeHoverCSSRule() {
      if ('createTouch' in document) {
        try {
          var ignore = /:hover/;
          for (var i = 0; i < document.styleSheets.length; i++) {
            var sheet = document.styleSheets[i];
            if (!sheet.cssRules) {
              continue;
            }
            for (var j = sheet.cssRules.length - 1; j >= 0; j--) {
              var rule = sheet.cssRules[j];
              if (rule.type === CSSRule.STYLE_RULE && ignore.test(rule.selectorText)) {
                sheet.deleteRule(j);
              }
            }
          }
        }
        catch(e) {
        }
      }
    }

    removeHoverCSSRule();

    var cycleImages = true;

    var initSlideShow = function() {
        if($($('#main-header')[0]).hasClass('slide-show')) {
            var backgrounds = ['child-2', 'child-3', 'child-6', 'hospital-1'],
                backgroundIndex = 0,
                activeBackground = backgrounds[backgroundIndex];


            var myInterval = setInterval(function(){ myTimer() }, 10000);

            function myStopTimer() {
                clearInterval(myInterval);
            }

            function myTimer() {
                if($($('#main-header')[0]).hasClass('slide-show') && cycleImages) {
                    console.log("remove " + activeBackground);
                    document.querySelectorAll("#main-header")[0].classList.remove(activeBackground);
                    if(backgroundIndex < backgrounds.length - 1) {
                        backgroundIndex = backgroundIndex + 1;
                        activeBackground = backgrounds[backgroundIndex];
                    } else {
                        backgroundIndex = 0;
                        activeBackground = backgrounds[backgroundIndex];
                    }

                    console.log("add " + activeBackground);
                    document.querySelectorAll("#main-header")[0].classList.add(activeBackground);
                } else {
                    myStopTimer()   
                }
            }
        }
    }

    var timeout = window.setTimeout(function() {
        initSlideShow();
    }, 1000);




})(); // Anonymous function