// Include gulp

// Include Our Plugins
// var jshint = require('gulp-jshint');
// var sourcemaps = require('gulp-sourcemaps');
// var concat = require('gulp-concat');
// var uglify = require('gulp-uglify');
var rename = require('gulp-rename');

var handlebars = require('gulp-handlebars');
// var handlebars = require('gulp-compile-handlebars');
// var rename = require('gulp-rename');
var wrap = require('gulp-wrap'); // handlebars dependency
var declare = require('gulp-declare'); // handlebars dependency
var concat = require('gulp-concat'); // handlebars dependency

var gulp = require('gulp');

var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();
var reload      = browserSync.reload;

var sassOptions = {
    errLogToConsole: true,
    outputStyle: 'expanded'
};

var themes = ['theme1', 'theme2', 'theme3'];

// Compile Our Sass
gulp.task('sass', function() {
        gulp
            .src('css/*.scss')
            .pipe(sourcemaps.init())
            .pipe(sass(sassOptions).on('error', function (err) {
                console.log(err.message + ' on line ' + err.lineNumber + ' in file : ' + err.fileName);
            }))
            .pipe(sourcemaps.write())
            .pipe(autoprefixer())
            // .pipe(rename('style1.css'))
            .pipe(gulp.dest('css/'))
            .pipe(reload({stream: true}));
});

// Auto Refresh Browser
gulp.task('browser-sync', function() {
    browserSync.init({
        browser: "google chrome",
        server: {
            baseDir: ""
        },
        port: 4000
    });
});

// gulp.task('templates', function () {
//     var templateData = {
//         firstName: 'Kaanon'
//     },
//     options = {
//         ignorePartials: true, //ignores the unknown footer2 partial in the handlebars template, defaults to false 
//         partials : {
//             footer : '<footer>the end</footer>'
//         },
//         batch : ['templates'],
//         helpers : {
//             capitals : function(str){
//                 return str.toUpperCase();
//             }
//         }
//     }
 
//     return gulp.src('templates/about.handlebars')
//         .pipe(handlebars(templateData, options))
//         .pipe(rename('hello.html'))
//         .pipe(gulp.dest('dist'));
// });

gulp.task('templates', function(){
  gulp.src('templates/*.hbs')
    .pipe(handlebars())
    .pipe(wrap('Handlebars.template(<%= contents %>)'))
    .pipe(declare({
      namespace: 'MyApp.templates',
      noRedeclare: true, // Avoid duplicate declarations 
    }))
    .pipe(concat('templates.js'))
    .pipe(gulp.dest('build/js/'));
});

// Watch Files For Changes
gulp.task('watch', function() {
    // gulp.watch('js/*.js', ['scripts']);
    gulp.watch('css/*.scss', ['sass']);
    gulp.watch(['*.json', 'data/*.json'], browserSync.reload);
    gulp.watch(['*.html', 'html/*.html'], browserSync.reload);
    gulp.watch(['*.js', 'js/*.js'], browserSync.reload);
    gulp.watch(['*.handlebars', 'html/**/*.handlebars'], browserSync.reload);
});

gulp.task('default', ['sass', 'watch', 'browser-sync', 'templates']);

// Concatenate & Minify JS
// gulp.task('scripts', function() {
//     return gulp.src('js/*.js')
//         // .pipe(concat('all.js'))
//         .pipe(gulp.dest('dist'))
//         // .pipe(rename('all.min.js'))
//         // .pipe(uglify())
//         .pipe(gulp.dest('dist'));
// });

// Watch Files For Changes
// gulp.task('html-watch', function() {
//     gulp.watch('js/*.js', ['scripts']);
//     gulp.watch('*.html', ['html']);
// });

// gulp.task('partials', function() {
//   // Assume all partials start with an underscore 
//   // You could also put them in a folder such as source/templates/partials/*.hbs 
//   gulp.src(['templates/_*.hbs'])
//     .pipe(handlebars())
//     .pipe(wrap('Handlebars.registerPartial(<%= processPartialName(file.relative) %>, Handlebars.template(<%= contents %>));', {}, {
//       imports: {
//         processPartialName: function(fileName) {
//           // Strip the extension and the underscore 
//           // Escape the output with JSON.stringify 
//           return JSON.stringify(path.basename(fileName, '.js').substr(1));
//         }
//       }
//     }))
//     .pipe(concat('partials.js'))
//     .pipe(gulp.dest('build/js/'));
// });

// Lint Task
// gulp.task('lint', function() {
//     return gulp.src('js/*.js')
//         .pipe(jshint())
//         .pipe(jshint.reporter('default'));
// });

// Default Task